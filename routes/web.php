<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/genrules', 'RuleController@rules')->name('genrules');
Route::get('/entity', 'RuleController@entity')->name('entity');
Route::post('/ajaxaddfactor', 'RuleController@addfactor')->name('ajaxaddfactor');
Route::post('/ajaxaddapprover', 'RuleController@ajaxaddapprover')->name('ajaxaddapprover');
Route::post('factorimportExcel', 'RuleController@factorimportExcel')->name('factorimportExcel');
Route::post('approversimportExcel', 'RuleController@approversimportExcel')->name('approversimportExcel');