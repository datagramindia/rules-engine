@extends('layouts.app')

@section('content')
<div class="container">
<div id="approvingpersons" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Setup Approvers</h4>
      </div>
      <div class="modal-body">
        <label>Approver ID(As per External System)</label>
        <input id="approverid" type="number" class="form-control" name="dfactor">
        <label>Approver Details</label>
        <input id="dapprovername" type="text" class="form-control" name="dfactor">

<label>Or</label>

<form role='form' action="{{ route('approversimportExcel') }}" enctype="multipart/form-data" method="POST">
         
            {{ csrf_field() }}
    <label>Upload Factors</label>
    <input id="" type="file" name="approverfile">
    <button type="submit" class="btn btn-success">Upload Approvers</button> 
</form>
        
      </div>
      <div class="modal-footer">
        <button onclick="saveapprovers()" type="button" class="btn btn-warning" >Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="factorsetup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Setup Deciding Factor</h4>
      </div>
      <div class="modal-body">
        <label>Deciding Factor Name</label>
        <input id="dfactor" type="text" class="form-control" name="dfactor">

<label>Or</label>

<form role='form' action="{{ route('factorimportExcel') }}" enctype="multipart/form-data" method="POST">
         
            {{ csrf_field() }}
    <label>Upload Factors</label>
    <input id="" type="file" name="dfactorfile">
    <button type="submit" class="btn btn-success">Upload</button> 
</form>
        
      </div>
      <div class="modal-footer">
        <button onclick="savedecidingfactor()" type="button" class="btn btn-warning" >Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <div class="row">
        <div class="col-md-4 ">
            <div class="panel panel-default">
                <div class="panel-heading">Factors</div>

                <div class="panel-body" id="factorarea">
                    
                @foreach($factor as $key=>$val)
                    {{$val->factorname}}
                    <hr>
                @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-4 ">
            <div class="panel panel-default">
                <div class="panel-heading">Entity</div>

                <div class="panel-body" id="approverarea">
                  

                    @foreach($approver as $key=>$val)
                    {{$val->approvername}}({{$val->approverid}})
                    <hr>
                @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-4 ">
            <div class="panel panel-default">
                <div class="panel-heading">Levels</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>

    <script src="{{ asset('js/toast.js') }}"></script>
<script type="text/javascript">
    function savedecidingfactor() {
        var dfactor=$('#dfactor').val();
    if(dfactor!=""){
    $.ajaxSetup({
       headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf_token"]').attr('content')
            }
    });

     $.ajax({
               type:'POST',
               url:'{{url("/ajaxaddfactor")}}',
               data: {
                      "_token": "{{ csrf_token() }}",
                      dfactor: dfactor                    
                     },

             
               success:function(data) {
                
    alert("Saved Successfully");
    $("#factorarea").append(data.factorname+"<hr>");
                   

               }

             });



 }
 else
 {
    alert("Please enter some values to save.");
 }
    }

    function saveapprovers() {
        var approverid=$('#approverid').val();
        var dapprovername=$('#dapprovername').val();
    if(approverid!="" && dapprovername!=""){
    $.ajaxSetup({
       headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf_token"]').attr('content')
            }
    });

     $.ajax({
               type:'POST',
               url:'{{url("/ajaxaddapprover")}}',
               data: {
                      "_token": "{{ csrf_token() }}",
                      dapprovername: dapprovername,                  
                      approverid: approverid,                  
                     },

             
               success:function(data) {
                
    alert(data.approvername+"("+data.approverid+") Saved Successfully");
    $("#approverarea").append(data.approvername+"("+data.approverid+")<hr>");
                   

               }

             });



 }
 else
 {
    alert("Please enter some values to save.");
 }
    }
</script>


@endsection

