<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\factor;
use App\approver;
use DB;
use Excel;

use Illuminate\Support\Facades\Input;


class RuleController extends Controller
{
    
    public function ajaxaddapprover(Request $request)
    {
        //$fac=approver::where('factorname')->first();
    	$approver=new approver();
    	$approver->approverid=$request->approverid;
    	$approver->approvername=$request->dapprovername;
    	$approver->save();

        return response()->json($approver,201);
    }
    
    public function addfactor(Request $request)
    {
        $fac=factor::where('factorname')->first();
    	$factor=new factor();
    	$factor->factorname=$request->dfactor;
    	$factor->save();

        return response()->json($factor,201);
    }
    public function factorimportExcel(Request $request)
    {

        $request->validate([

            'dfactorfile' => 'required'

        ]);

 
        $path = $request->file('dfactorfile')->getRealPath();
        $data = Excel::load($path)->get();
      //  dd($data);
        if($data->count()){
            foreach ($data as $key => $value) {
            $arr[] = ['factorname' => $value->factorname];
            }
            if(!empty($arr)){
         //   	dd($arr);
            factor::insert($arr);
            }

            }

 

        return back();

    

    }
    public function approversimportExcel(Request $request)
    {

        $request->validate([

            'approverfile' => 'required'

        ]);

 
        $path = $request->file('approverfile')->getRealPath();
        $data = Excel::load($path)->get();
      //  dd($data);
        if($data->count()){
            foreach ($data as $key => $value) {
            $arr[] = ['approverid' => $value->approverid,'approvername' => $value->approvername];
            }
            if(!empty($arr)){
         //   	dd($arr);
            approver::insert($arr);
            }

            }

 

        return back();

    

    }
    public function rules()
    {
        return view('home');
    }
}
