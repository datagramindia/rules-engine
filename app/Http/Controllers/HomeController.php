<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\factor;
use App\approver;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $factor=factor::get();
        $approver=approver::get();
        return view('home',compact('factor','approver'));
    }
}
